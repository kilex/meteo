<!DOCTYPE html>
<html>
<head>
    <title>Meteo</title>
    <meta charset="utf-8">
    <meta http-equiv="refresh" content="300">
    <link href="dist/css/bootstrap.css" rel="stylesheet">
    <link href="dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="dist/css/bootstrap-theme.css" rel="stylesheet">

    <!--    http://realfavicongenerator.net-->

<style>
    .container {
        /*width: 100%;*/
        /*max-width: 1050px;*/
        /*min-height: 1000px;*/
    }
</style>

</head>
<body>

<div class="container">

    <?php
    if (!$_GET[radar]) $_GET[radar]='UVKIzhevsk';
    $radars=array(
        'UVKIzhevsk'=>'Ижевск',
        'UVKKazan'=>'Казань',
        'UVKKirov'=>'Киров',
        'UVKSochi'=>'Сочи',
        'UVKOrenburg'=>'Оренбург',
//        'UVKKrasnodar'=>'Краснодар',
//        'UVKProfsoyuz'=>'Москва',
    );

    $namem=$radars[$_GET[radar]];
    print "<h3>Метеообстановка $namem</h3>";

    foreach ( $radars as $one=>$name) {
//        print $one.$name;
        $class='primary';
        if ($one==$_GET[radar]) $class=success;
        if ($one=='UVKIzhevsk') print '<a class="btn btn-sm btn-'.$class.'" href="http://meteo.kilex.ru/">'.$name.'</a> ';
        else print '<a class="btn btn-sm btn-'.$class.'" href="http://meteo.kilex.ru/?radar='.$one.'">'.$name.'</a> ';
    }
    ?>

    <a class="btn btn-sm btn-warning" href="http://meteo.kilex.ru/flood.php">Пожелания</a>
    <!--<a class="btn btn-sm btn-success"  href="http://meteo.kilex.ru/?radar=RUDZ">Казань</a>-->
    <!--<a class="btn btn-sm btn-success"  href="http://meteo.kilex.ru/?radar=RUSI">Сочи</a>-->
    <!--<a class="btn btn-sm btn-success"  href="http://meteo.kilex.ru/?radar=RUDE">Москва</a>-->
<!--    <a class="btn btn-sm btn-success" href="http://meteo.kilex.ru/?radar=UVKIzhevsk">Ижевск</a>-->
<!--    <a class="btn btn-sm btn-success"  href="http://meteo.kilex.ru/?radar=UVKKazan">Казань</a>-->
<!--    <a class="btn btn-sm btn-success"  href="http://meteo.kilex.ru/?radar=UVKKirov">Киров</a>-->
<!--    <a class="btn btn-sm btn-success"  href="http://meteo.kilex.ru/?radar=UVKSochi">Сочи</a> -->
    <br>Если нужны еще радары - пишите мне в <a href="https://twitter.com/kilexst">twitter</a> или в раздел предложения, добавлю
    <!--<a class="btn btn-sm btn-success"  href="http://meteo.kilex.ru/?radar=RUDE">Москва</a>-->
    <br>
    <br>
    <!--<div id="picture" class=""></div>-->

<!--    <img src="./UVKKazan.gif">-->
<!--    <img src="./UVKSamara.gif">-->
<!--    <img src="./UVKSochi.gif">-->
    <?php

        print '<a href="./'.$_GET[radar].'-f.gif"><img width="800" src="./'.$_GET[radar].'.gif"></a> <img src="legend.png" alt="Легенда"> <br> <a href="./'.$_GET[radar].'-f.gif" class="btn btn-sm btn-warning">История</a>';
//    else print '<img src="./UVKIzhevsk.gif">';

    ?>

    <br>
    <br>
    <p>Анимация текущей обстановки в небе над Удмуртией (и не только). Данные отображаются за последние 5 часов. Возможно отставание, тк данные из источника всегда запоздавшие. Чтобы посмотреть более детальную и давнюю картинку - нажимайте на кнопку история или на саму карту</p>
    <!--<p>Информация нихрена не любезно взята <a href="http://meteoinfo.by/radar/?q=RUDI&t=0" target="_blank">отсюда</a> В самом деле они сами не в состоянии сделать анимацию и блокируют доступ к своим данным с подобных ресурсов. <b>Так что лучи поноса им в карму!!!!!</b></b></p>-->
    <p>Информация любезно взята <a href="http://www.meteorad.ru/data/uvkI.html" target="_blank">отсюда</a><br><b>Отдельное спасибо <a href="https://twitter.com/AlxYu">AlxYu</a> за предоставленый код автоматизации сбора гифок</b></p>
    <p>Буду очень признателен любым донатам, кнопка ниже</p>
    <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
        <input type="hidden" name="cmd" value="_s-xclick">
        <input type="hidden" name="encrypted" value="-----BEGIN PKCS7-----MIIHPwYJKoZIhvcNAQcEoIIHMDCCBywCAQExggEwMIIBLAIBADCBlDCBjjELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAkNBMRYwFAYDVQQHEw1Nb3VudGFpbiBWaWV3MRQwEgYDVQQKEwtQYXlQYWwgSW5jLjETMBEGA1UECxQKbGl2ZV9jZXJ0czERMA8GA1UEAxQIbGl2ZV9hcGkxHDAaBgkqhkiG9w0BCQEWDXJlQHBheXBhbC5jb20CAQAwDQYJKoZIhvcNAQEBBQAEgYBCTXL+o611ftrBXLU9z54xNeozGra9J9xcf3C1EKsBeneCwHSslsU/JbSUV8KFWgeMZTuHIFUDwcsI+5SkNiiao+ra3+oTjHVr042ZPM+w09oaaN14GfPayBqkwYEjc+O3UzBx7P7SNg6gs1jzpwn1XgU6WE3tXVP56nEI7KaIZTELMAkGBSsOAwIaBQAwgbwGCSqGSIb3DQEHATAUBggqhkiG9w0DBwQI2yj7uTCkEh+AgZhquHpz2usu7bET8bjkPnAAzNl/lyenni6oeAqUqHamnwc8Nl0dupFIUEytZRKVpyKjiuaC+394dm3hqCrAY0dWBl8MmktSYiUpylJaIS4ACO2SXq10GRd5w2jv9U3MBjyPCcWvST1Nc+jV/Bqqvtg2oOs0J2s+lf1AtIp40R3An0sXA4uFRkvRHFQKet97gr1FZbKJ0LaJaaCCA4cwggODMIIC7KADAgECAgEAMA0GCSqGSIb3DQEBBQUAMIGOMQswCQYDVQQGEwJVUzELMAkGA1UECBMCQ0ExFjAUBgNVBAcTDU1vdW50YWluIFZpZXcxFDASBgNVBAoTC1BheVBhbCBJbmMuMRMwEQYDVQQLFApsaXZlX2NlcnRzMREwDwYDVQQDFAhsaXZlX2FwaTEcMBoGCSqGSIb3DQEJARYNcmVAcGF5cGFsLmNvbTAeFw0wNDAyMTMxMDEzMTVaFw0zNTAyMTMxMDEzMTVaMIGOMQswCQYDVQQGEwJVUzELMAkGA1UECBMCQ0ExFjAUBgNVBAcTDU1vdW50YWluIFZpZXcxFDASBgNVBAoTC1BheVBhbCBJbmMuMRMwEQYDVQQLFApsaXZlX2NlcnRzMREwDwYDVQQDFAhsaXZlX2FwaTEcMBoGCSqGSIb3DQEJARYNcmVAcGF5cGFsLmNvbTCBnzANBgkqhkiG9w0BAQEFAAOBjQAwgYkCgYEAwUdO3fxEzEtcnI7ZKZL412XvZPugoni7i7D7prCe0AtaHTc97CYgm7NsAtJyxNLixmhLV8pyIEaiHXWAh8fPKW+R017+EmXrr9EaquPmsVvTywAAE1PMNOKqo2kl4Gxiz9zZqIajOm1fZGWcGS0f5JQ2kBqNbvbg2/Za+GJ/qwUCAwEAAaOB7jCB6zAdBgNVHQ4EFgQUlp98u8ZvF71ZP1LXChvsENZklGswgbsGA1UdIwSBszCBsIAUlp98u8ZvF71ZP1LXChvsENZklGuhgZSkgZEwgY4xCzAJBgNVBAYTAlVTMQswCQYDVQQIEwJDQTEWMBQGA1UEBxMNTW91bnRhaW4gVmlldzEUMBIGA1UEChMLUGF5UGFsIEluYy4xEzARBgNVBAsUCmxpdmVfY2VydHMxETAPBgNVBAMUCGxpdmVfYXBpMRwwGgYJKoZIhvcNAQkBFg1yZUBwYXlwYWwuY29tggEAMAwGA1UdEwQFMAMBAf8wDQYJKoZIhvcNAQEFBQADgYEAgV86VpqAWuXvX6Oro4qJ1tYVIT5DgWpE692Ag422H7yRIr/9j/iKG4Thia/Oflx4TdL+IFJBAyPK9v6zZNZtBgPBynXb048hsP16l2vi0k5Q2JKiPDsEfBhGI+HnxLXEaUWAcVfCsQFvd2A1sxRr67ip5y2wwBelUecP3AjJ+YcxggGaMIIBlgIBATCBlDCBjjELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAkNBMRYwFAYDVQQHEw1Nb3VudGFpbiBWaWV3MRQwEgYDVQQKEwtQYXlQYWwgSW5jLjETMBEGA1UECxQKbGl2ZV9jZXJ0czERMA8GA1UEAxQIbGl2ZV9hcGkxHDAaBgkqhkiG9w0BCQEWDXJlQHBheXBhbC5jb20CAQAwCQYFKw4DAhoFAKBdMBgGCSqGSIb3DQEJAzELBgkqhkiG9w0BBwEwHAYJKoZIhvcNAQkFMQ8XDTE1MDYyNTA4MjI0NFowIwYJKoZIhvcNAQkEMRYEFP1Hw1hwyUPk4l6Z8Lp3TcMR1XTTMA0GCSqGSIb3DQEBAQUABIGAtbiVEV2G5Fs/Z6YfiH3ZWXfsP4JI/1CpEVx+B/w8pre5roxmWoMvp0pvTFQc1aSTcB1w0M1EVwQL/I3dfgTELNfIjdJVgCsY8Bajcq7cYBubvzVlK6gnWEY2kfX7Ahe99PcSMVgTX1La3U+QmV1zlgt6jdXbv9pCiRJ1d6nsw7I=-----END PKCS7-----
">
        <input type="image" src="https://www.paypalobjects.com/ru_RU/RU/i/btn/btn_donateCC_LG.gif" border="0" name="submit" alt="PayPal — более безопасный и легкий способ оплаты через Интернет!">
        <img alt="" border="0" src="https://www.paypalobjects.com/ru_RU/i/scr/pixel.gif" width="1" height="1">
    </form>


    <p>По анимации можно оценить погоду в городе <?php print $namem ?> - по движению синих и красных тучек становится понятно будет ли дождик и гроза в интересующем районе</p>
    <p>Погода в <?php print $namem ?>, осадки в <?php print $namem ?>, будет ли дождь в <?php print $namem ?>, бесплатно без смс и регистрации!</p>


    <div id="disqus_thread"></div>
    <script type="text/javascript">
        /* * * CONFIGURATION VARIABLES * * */
        var disqus_shortname = 'meteok';

        /* * * DON'T EDIT BELOW THIS LINE * * */
        (function() {
            var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
            dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
            (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
        })();
    </script>
    <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>

</div>

<script src="dist/js/jquery.js"></script>
<script src="dist/js/bootstrap.js"></script>
<script src="dist/js/bootstrap.min.js"></script>
<script src="dist/js/tab.js"></script>
<script src="dist/js/dropdown.js"></script>
<script src="dist/js/button.js"></script>

<script>
    var unixtimes;
    var selected;
    var selected_num;
    var sel_length;

    $( document ).ready(function() {
//        console.log( "ready!" );
//        gettimes();


//  Обновлялка списка тейлов 
//        var tilist= setTimeout(timesnext,240000);
//        function timesnext() {
//            gettimes();
//            var tilist= setTimeout(timesnext,240000);
//        }
    });

//
//    function gettimes() {
////        console.log("getting unixtimes...");
//        $.ajax({
//            type: "GET",
//            url: "list.php",
//            async: true,
//            dataType: "json",
//            success: function(data){
////                console.log("getted!");
//                unixtimes=data;
//                sel_length=unixtimes.length;
//                selected_num=0;
//                selected=unixtimes[selected_num];
//                showPic();
//                var tid = setTimeout(next, 2000);
//            }
//        });
//    }
//
//        function showPic() {
//
//
//            var date = new Date(selected*1000);
//            var formattedTime=date.toLocaleString();
//
//            var datestart=new Date(unixtimes[0]*1000);
//            var formattedTimeStart=datestart.toLocaleString();
//
//            var datefinish=new Date(unixtimes[sel_length-1]*1000);
//            var formattedTimeFinish=datefinish.toLocaleString();
//
//            var radar = getParameterByName('radar');
//            if (radar == '') radar='RUDI';
//
//            $("#picture").html("Диапазон: <p class='label label-success'>"+formattedTimeStart+"</p> - <p class='label label-success'>"+formattedTimeFinish+"</p><br>" +
//                    "Текущая позиция: <p class='label label-primary'>"+formattedTime+"</p><br><br>"+
////                    "<img src='http://meteo.kilex.ru/radar/RUDI/RUDI_"+selected+".png'</img>"+
//                    "<center><img src='http://meteoinfo.by/radar/"+radar+"/"+radar+"_"+selected+".png'</img></center>"
////                    "<img src='http://meteo.kilex.ru/radar/RUDI/RUDI_"+selected+"_71.png'</img>"
////                    "<img src='http://meteoinfo.by/radar/"+radar+"/"+radar+"_"+selected+"_71.png'</img>"
////                    "<img src='http://meteoinfo.by/radar/RUDI/RUDI_"+selected+"_78.png'</img>"+
////                    "<img src='http://meteoinfo.by/radar/RUDI/RUDI_"+selected+"_81.png'</img>"
//            );
//        }
//
//        function next() {
////            console.log("next! "+selected_num);
//
//            selected_num++;
//            var timer=200;
//            if (selected_num>=sel_length) {
//                selected_num=0;
//
//            }
//            if (selected_num===sel_length-1) {
//                timer=3000;
//            }
//            selected=unixtimes[selected_num];
////            console.log("selected - "+selected);
//            showPic();
//            tid = setTimeout(next, timer);
//        }
//
//    function getParameterByName(name) {
//        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
//        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
//                results = regex.exec(location.search);
//        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
//    }
//
//
//
//</script>

<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-51757450-1', 'kilex.ru');
    ga('send', 'pageview');

</script>

</body>
</html>
