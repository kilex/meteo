#!/bin/bash
#meteo=/www/html/statserv/pic
meteo=/var/www/meteorad

for RADAR in UVKIzhevsk UVKKazan UVKSochi UVKKirov UVKOrenburg #UVKKrasnodar UVKProfsoyuz
	do

		file=$RADAR
		filehi=history/$RADAR
		anim=$meteo/$RADAR
		animhi=$meteo/$filehi
		
		cd $meteo
		
			echo "$RADAR convert to full";
			date
		    time convert  -delay 15 $RADAR-*.png +repage -loop 0 -reverse +dither -treedepth 1 -colors 25 $anim-f.gif
			echo "$RADAR add timeouts";
			date
		    time convert -layers OptimizeTransparency $anim-f.gif \( -clone 0 -set delay 100 \) -swap 0 +delete \( +clone -set delay 300 \) +swap +delete $anim-f.gif
			date
			cp $anim-f.gif $anim.gif
			echo "$RADAR done";
			date
			cp $anim.gif /var/www/meteo/
done
