#!/bin/bash
#meteo=/www/html/statserv/pic
meteo=/var/www/meteorad

for RADAR in UVKIzhevsk UVKKazan UVKKirov UVKSochi UVKOrenburg #UVKKrasnodar UVKProfsoyuz
	do

		file=$RADAR
		filehi=history/$RADAR
		anim=$meteo/$RADAR
		animhi=$meteo/$filehi
		date=$(date +"%d-%m-%Y %T")
		
		cd $meteo
		
		wget --quiet -N  http://www.meteorad.ru/data/$file.png
		
		new=$(cmp $file.png $file+last.png)

		if [ $? != 0 ] ; then
		    cp $file.png $file+last.png
			mv $file.png $file+clean.png
#			FILEDATE=$(stat $file.png --printf=%y)
#			montage -geometry +0+0 -background white -label "$FILEDATE" $file-clean.png $file.png
			montage -crop 1150x950+185+55 -geometry +0+0 -background white -label "meteo.kilex.ru - $date" $file+clean.png $file.png
			rm $file+clean.png

		    for i in {29..0}; do mv $file-$(printf %02d $i).png $file-$(printf %02d $((i+1))).png; done
		    for i in {300..0}; do mv $filehi-$(printf %03d $i).png $filehi-$(printf %03d $((i+1))).png; done
		    cp $file.png $file-00.png
		    cp $file.png $filehi-000.png
			rm $file.png
		fi
done
