#!/bin/bash
#meteo=/www/html/statserv/pic
meteo=/var/www/meteorad

for RADAR in UVKIzhevsk UVKKazan UVKSochi UVKKirov UVKOrenburg #UVKKrasnodar UVKProfsoyuz
	do

		file=$RADAR
		filehi=history/$RADAR
		anim=$meteo/$RADAR
		animhi=$meteo/$filehi
		
		cd $meteo
		
			echo "$RADAR convert to history";
		    time convert  -delay 10 history/$RADAR-*.png +repage -loop 0 -reverse +dither -treedepth 1 -colors 25 $animhi-f.gif
		    time convert -layers OptimizeTransparency $animhi-f.gif \( -clone 0 -set delay 100 \) -swap 0 +delete \( +clone -set delay 100 \) +swap +delete $animhi-f.gif
			echo "$RADAR done";
			cp $animhi-f.gif /var/www/meteo/
done
